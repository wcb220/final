all: main
	mkdir -p build/bin
	mkdir -p build/lib
	mkdir -p build/objects
	mv *.o build/objects
	mv *.a build/lib
	mv main build/bin

main: main.o libhashset.a liblinkedlist.a
	gcc main.o -o main -lhashset -llinkedlist -L. -Llib

main.o: src/bin/main.c
	gcc src/bin/main.c -c -I include -I lib

libhashset.a: src/libhashset.c
	gcc src/libhashset.c -c -I include -I lib
	ar rs libhashset.a libhashset.o

liblinkedlist.a: src/liblinkedlist.c
	gcc src/liblinkedlist.c -c -I include -I lib
	ar rs liblinkedlist.a liblinkedlist.o

libbenchmarker.a: src/lib.c
	gcc src/lib.c -c -I include -I lib
	ar rs libbenchmarker.a libbenchmarker.o

clean:
	rm -rf build
	rm -f main
	rm -f main.o
	rm -f liblinkedlist.a
	rm -f liblinkedlist.o
	rm -f libhashset.a
	rm -f libhashset.o