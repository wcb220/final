#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "hashset.h"
#include "linkedlist.h"

// Generate a prehash for an item with a given size
unsigned long prehash(void* item, unsigned int item_size) {
    unsigned long h = 5381;
    char *c = item;
    for (int i = 0; i < item_size; i++) { 
        h = ((h << 5) + h) + c[i];
    }
    return h;
}

// Hash an unsigned long into an index that fits into a hash set
unsigned long hash(unsigned long prehash, unsigned long buckets) {
    return prehash % buckets;
}

// Initialize an empty hash set with a given size
void initHashSet(HashSet* hashset_pointer, unsigned int size) {
    hashset_pointer->size = size;
    hashset_pointer->load_factor = 0;
    hashset_pointer->table = (List**) malloc(sizeof(List*) * size);
    for (int i = 0; i < size; i++) {
        List* list = (List *) malloc(sizeof(List));
        initList(list);
        hashset_pointer->table[i] = list;
    }

}

// Insert item in the set. Return true if the item was inserted, false if it wasn't (i.e. it was already in the set)
// Recalculate the load factor after each successful insert (round to nearest whole number).
// If the load factor exceeds 70 after insert, resize the table to hold twice the number of buckets.
bool insertItem(HashSet* hashset_pointer, void* item, unsigned int item_size) {
    unsigned long my_hash = hash(prehash(item, item_size), hashset_pointer->size);
    if (findItem(hashset_pointer, item, item_size)) {
        return false;
    }
    Node* node = malloc(sizeof(Node*));
    node = createNode(prehash(item, item_size));
    insertAtTail(hashset_pointer->table[my_hash], prehash(item, item_size));
    int filled_buckets = 0;
    for (int i = 0; i < hashset_pointer->size; i++) {
        if (hashset_pointer->table[i]->head != NULL) {
            filled_buckets++;
        }
    }
    filled_buckets *= 100;
    hashset_pointer->load_factor = filled_buckets / hashset_pointer->size;
    if (hashset_pointer->load_factor >= 70) {
        resizeTable(hashset_pointer, hashset_pointer->size * 2);
    }
    return true;
}

// Remove an item from the set. Return true if it was removed, false if it wasn't (i.e. it wasn't in the set to begin with)
bool removeItem(HashSet* hashset_pointer, void* item, unsigned int item_size) {
    unsigned long my_hash = hash(prehash(item, item_size), hashset_pointer->size);
    if (!findItem(hashset_pointer, item, item_size)) {
        return false;
    }
    Node* node;
    List* list = hashset_pointer->table[my_hash];
    if (list->head != NULL) {
        node = list->head;
        int index = 0;
        
        do {
            if (node->item == prehash(item, item_size)) {
                if (index == 0) {
                    removeHead(list);
                } else {
                    removeAtIndex(list, index);
                }
                return true;
            } else {
                index++;
                node = node->next;
            }
        } while (node != NULL);
    }
    return false;
}

// Return true if the item exists in the set, false otherwise
bool findItem(HashSet* hashset_pointer, void* item, unsigned int item_size) {
    unsigned long my_hash = hash(prehash(item, item_size), hashset_pointer->size);
    if (hashset_pointer->table[my_hash]->head != NULL) {
        Node* node = hashset_pointer->table[my_hash]->head;
        while (node != NULL) {
            if (node->item == prehash(item, item_size)) {
                return true;
            } else {
                node = node->next;
            }
        }
    }
    return false;
}

// Resize the underlying table to the given size. Recalculate the load factor after resize
void resizeTable(HashSet* hashset_pointer, unsigned int new_size) {
    HashSet* hp_temp;
    hp_temp = (HashSet*) malloc(sizeof(HashSet));
    initHashSet(hp_temp, new_size);
    for (int i = 0; i < hashset_pointer->size; i++) {
        Node* node;
        List* list = hashset_pointer->table[i];
        if (list->head != NULL) {
            node = list->head;
            do {
                Node* node2 = malloc(sizeof(Node*));
                node2 = createNode(node->item);
                insertAtTail(hp_temp->table[hash(node->item, hp_temp->size)], node->item);
                removeHead(list);
                node = list->head;
            } while (node != NULL);
        }
        free(list);
    }
    free(hashset_pointer->table);
    hashset_pointer->size = hp_temp->size;
    hashset_pointer->table = hp_temp->table;
    int filled_buckets = 0;
    for (int i = 0; i < hashset_pointer->size; i++) {
        if (hashset_pointer->table[i]->head != NULL) {
            filled_buckets++;
        }
    }
    filled_buckets *= 100;
    hashset_pointer->load_factor = filled_buckets / hashset_pointer->size;    
}

// Print Table
void printHashSet(HashSet* hashset_pointer) {
    for (int i = 0; i < hashset_pointer->size; i++) {
        List* list = hashset_pointer->table[i];
        printList(list);
    }
}