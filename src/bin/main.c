#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>	
#include <unistd.h>	
#include "linkedlist.h"
#include "hashset.h"
#define BILLION  1000000000.0

int main(int argc, char* argv[]){

    if (argv[1] == NULL){
        printf("%s\n", "No File");
        return 0;
    }

    int count = 0;

    HashSet *hs1e3 = (HashSet*) malloc(sizeof(HashSet));
    HashSet *hs1e4 = (HashSet*) malloc(sizeof(HashSet));
    HashSet *hs1e5 = (HashSet*) malloc(sizeof(HashSet));
    HashSet *hs1e6 = (HashSet*) malloc(sizeof(HashSet));
    List *list = (List*) malloc(sizeof(List));
	

    struct timespec start, end;
	//clock_gettime(CLOCK_REALTIME, &start);
    //clock_gettime(CLOCK_REALTIME, &end);

    printf("%s\n", "Average Insertion Time");
    printf("%28s", "HashSet 1e3");
    printf("%25s", "HashSet 1e4");
    printf("%25s", "HashSet 1e5");
    printf("%25s", "HashSet 1e6");
    printf("%25s", "LinkedList");

    FILE *file;
    char *fileSize;
    char line[200];

    double hs1e3_insert_time;
    double hs1e4_insert_time;
    double hs1e5_insert_time;
    double hs1e6_insert_time;
    double list_insert_time;
    double hs1e3_seek_time[4];
    double hs1e4_seek_time[4];
    double hs1e5_seek_time[4];
    double hs1e6_seek_time[4];
    double list_seek_time[4];
    

    for (int i = 1; i < 5; i++){

        initHashSet(hs1e3, 1000);
        initHashSet(hs1e4, 10000);
        initHashSet(hs1e5, 100000);
        initHashSet(hs1e6, 1000000);
        initList(list);

        file = fopen(argv[i], "r");
    
        clock_gettime(CLOCK_REALTIME, &start);
        while ( fgets(line, sizeof(line), file) != NULL){
            insertItem(hs1e3, &line, sizeof(line));
            count++;
        }
        clock_gettime(CLOCK_REALTIME, &end);
        hs1e3_insert_time = ((end.tv_sec - start.tv_sec) * BILLION + (end.tv_nsec - start.tv_nsec))/count;

        rewind(file);

        clock_gettime(CLOCK_REALTIME, &start);
        while ( fgets(line, sizeof(line), file) != NULL){
            insertItem(hs1e4, &line, sizeof(line));
        }
        clock_gettime(CLOCK_REALTIME, &end);
        hs1e4_insert_time = ((end.tv_sec - start.tv_sec) * BILLION + (end.tv_nsec - start.tv_nsec))/count;

        rewind(file);

        clock_gettime(CLOCK_REALTIME, &start);
        while ( fgets(line, sizeof(line), file) != NULL){
            insertItem(hs1e5, &line, sizeof(line));
        }
        clock_gettime(CLOCK_REALTIME, &end);
        hs1e5_insert_time = ((end.tv_sec - start.tv_sec) * BILLION + (end.tv_nsec - start.tv_nsec))/count;

        rewind(file);

        clock_gettime(CLOCK_REALTIME, &start);
        while ( fgets(line, sizeof(line), file) != NULL){
            insertItem(hs1e6, &line, sizeof(line));
        }
        clock_gettime(CLOCK_REALTIME, &end);
        hs1e6_insert_time = ((end.tv_sec - start.tv_sec) * BILLION + (end.tv_nsec - start.tv_nsec))/count;

        rewind(file);

        clock_gettime(CLOCK_REALTIME, &start);
        while ( fgets(line, sizeof(line), file) != NULL){
            insertAtHead(list, &line);
        }
        clock_gettime(CLOCK_REALTIME, &end);
        list_insert_time = ((end.tv_sec - start.tv_sec) * BILLION + (end.tv_nsec - start.tv_nsec))/count;

        rewind(file);

        clock_gettime(CLOCK_REALTIME, &start);
        while ( fgets(line, sizeof(line), file) != NULL){
            findItem(hs1e3, &line, sizeof(line));
        }
        clock_gettime(CLOCK_REALTIME, &end);
        hs1e3_seek_time[i-1] = ((end.tv_sec - start.tv_sec) * BILLION + (end.tv_nsec - start.tv_nsec))/count;

        rewind(file);

        clock_gettime(CLOCK_REALTIME, &start);
        while ( fgets(line, sizeof(line), file) != NULL){
            findItem(hs1e4, &line, sizeof(line));
        }
        clock_gettime(CLOCK_REALTIME, &end);
        hs1e4_seek_time[i-1] = ((end.tv_sec - start.tv_sec) * BILLION + (end.tv_nsec - start.tv_nsec))/count;

        rewind(file);

        clock_gettime(CLOCK_REALTIME, &start);
        while ( fgets(line, sizeof(line), file) != NULL){
            findItem(hs1e5, &line, sizeof(line));
        }
        clock_gettime(CLOCK_REALTIME, &end);
        hs1e5_seek_time[i-1] = ((end.tv_sec - start.tv_sec) * BILLION + (end.tv_nsec - start.tv_nsec))/count;

        rewind(file);

        clock_gettime(CLOCK_REALTIME, &start);
        while ( fgets(line, sizeof(line), file) != NULL){
            findItem(hs1e6, &line, sizeof(line));
        }
        clock_gettime(CLOCK_REALTIME, &end);
        hs1e6_seek_time[i-1] = ((end.tv_sec - start.tv_sec) * BILLION + (end.tv_nsec - start.tv_nsec))/count;

        rewind(file);

        clock_gettime(CLOCK_REALTIME, &start);
        for (int i = 0; i < count; i++){
            itemAtIndex(list, i);
        }
        clock_gettime(CLOCK_REALTIME, &end);
        list_seek_time[i-1] = ((end.tv_sec - start.tv_sec) * BILLION + (end.tv_nsec - start.tv_nsec))/count;

        if (i==1){printf("\n%s", "1e2");}
        else if (i==2){printf("\n%s", "1e3");}
        else if (i==3){printf("\n%s", "1e4");}
        else if (i==4){printf("\n%s", "1e5");}


        printf("%25f", hs1e3_insert_time);
        printf("%25f", hs1e4_insert_time);
        printf("%25f", hs1e5_insert_time);
        printf("%25f", hs1e6_insert_time);
        printf("%25f", list_insert_time);

        count = 0;

    }
    
    printf("\n");
    

    count = 0;

    printf("\n%s\n", "Average Seek Time");
    printf("%28s", "HashSet 1e3");
    printf("%25s", "HashSet 1e4");
    printf("%25s", "HashSet 1e5");
    printf("%25s", "HashSet 1e6");
    printf("%25s", "LinkedList");

    for (int i = 1; i < 5; i++){

        if (i==1){printf("\n%s", "1e2");}
        else if (i==2){printf("\n%s", "1e3");}
        else if (i==3){printf("\n%s", "1e4");}
        else if (i==4){printf("\n%s", "1e5");}


        printf("%25f", hs1e3_seek_time[i-1]);
        printf("%25f", hs1e4_seek_time[i-1]);
        printf("%25f", hs1e5_seek_time[i-1]);
        printf("%25f", hs1e6_seek_time[i-1]);
        printf("%25f", list_seek_time[i-1]);
    }
    

    printf("\n");
}

